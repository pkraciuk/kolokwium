drop table if exists entries;
create table entries (
    id integer primary key autoincrement,
    user string not null,
    message string not null,
    timestamp datetime not null
);


drop table if exists users;
create table users (
  id integer primary key autoincrement,
  username string not null
)