# -*- coding: utf-8 -*-
import datetime

from flask import Flask, render_template
import sqlite3
from contextlib import closing
from flask import g, session
from flask import request, redirect, abort
from flask import flash, url_for
from flask.ext.couchdb import CouchDBManager
import requests

#################
#  Konfiguracja #
#################

DATABASE = 'microblog.db'
SECRET_KEY = '1234567890!@#$%^&*()'



#############
# Aplikacja #
#############

COUCHDB_SERVER = 'http://194.29.175.241:5984'
COUCHDB_DATABASE = 'chat'

app = Flask(__name__)
manager = CouchDBManager()
manager.setup(app)
app.config.from_object(__name__)


@app.route('/')
def show_messages():
    entries = get_all_messages()
    return render_template('show_entries.html', entries=entries)


@app.route('/entries')
def show_entries():
    entries = get_all_messages()
    return render_template('entries.html',entries=entries)


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if (len(get_user(request.form['username']))==0):
            print get_user(request.form['username'])
            try:
                do_login(request.form['username'])
            except ValueError:
                error = u"Blędne dane"
            else:
                flash(u'Zostales zalogowany')
                return redirect(url_for('show_messages'))
        else:
            error = 'Taki uzytkownik jest juz zalogowany'
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    do_logout(session['logged_in'])
    session.pop('logged_in', None)
    flash(u'Zostales wylogowany')
    return redirect(url_for('show_messages'))

@app.route('/add', methods=['GET','POST'])
def add_message():
    if request.method == 'GET':
        return redirect(url_for('show_messages'))
    print 'start'
    print request.form

    if not session.get('logged_in'):
        abort(401)
    if (request.form['message']!=''):
        try:
            time = str(datetime.datetime.now())
            send_message(session['logged_in'], request.form['message'], time)
            flash(u'Nowy wpis zostal dodany')
            #id = str(uuid.uuid4())
            #json_data = json.dumps({"user":session['logged_in'], "message":request.form['message'],"time":time,"id":id})
            #new_session = requests.session()
            #for link in get_links():
                #try:
                    #requests.post(link,data=json_data)
                #except:
                    #pass

        except sqlite3.Error as e:
            flash(u'Wystapil blad: %s' % e.args[0])
    else:
        flash(u'Nie mozna wyslac pustej wiadomosci')
    return redirect(url_for('show_messages'))


@app.route('/register')
def register():
    try:
        dataToRegisterApplication = {'host' : 'http://boiling-harbor-8958.herokuapp.com/', 'active' : True, 'delivery' : 'add' }
        g.couch['crackson3'] = dataToRegisterApplication
        g.couch.save(dataToRegisterApplication)
        flash(u'Aplikacja zostala zarejestrowana')
    except:
        flash(u'Aplikacja byla juz zarejestrowana')
    return redirect(url_for('show_messages'))


###############
# Baza danych #
###############

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    g.db.close()


def send_message(message, user, timestamp):
    g.db.execute('insert into entries (message, user, timestamp) values (?, ?, ?)',
                 [message, user, timestamp])
    g.db.commit()


def get_all_messages():
    cur = g.db.execute('select  message, user, timestamp from entries order by id desc')
    messages = [dict(user=row[0], text=row[1], timestamp=row[2]) for row in cur.fetchall()]
    return messages

def add_user(username):
    g.db.execute('insert into users (username) values (?)',
                 [username])
    g.db.commit()

def get_user(username):
    cur = g.db.execute('select username from users where username = ?',
                       [username])
    user = [dict(username=row[0]) for row in cur.fetchall()]
    return user

################
# Autentykacja #
################

def do_login(username):
    add_user(username)
    session['logged_in'] = username

def do_logout(username):
    print 'wylogowuje: ',username
    g.db.execute('delete from users where username = ?',[username])
    g.db.commit()

def get_links():
    url = "http://194.29.175.241:5984/chat/_design/utils/_view/list_active"
    session = requests.session()
    raw_data = session.get(url)
    new_data = raw_data.json()
    links = []
    i = 0
    for _ in new_data["rows"]:
        links.append(new_data["rows"][i]['value']['host'] + new_data["rows"][i]['value']['delivery'])
        i += 1
    return links

################
# Uruchomienie #
################

if __name__ == '__main__':
    try:
        dataToRegisterApplication = {'host' : 'http://boiling-harbor-8958.herokuapp.com/', 'active' : True, 'delivery' : 'add' }
        g.couch['crackson3'] = dataToRegisterApplication
        g.couch.save(dataToRegisterApplication)
    except:
        pass
    #init_db()
    app.run(debug=True)
